import { Args, Query, Resolver } from '@nestjs/graphql';
import { GraphQLString } from 'graphql';

import { MovieService } from './movie.service';
import { ImdbSearchItemModel } from './imdb/imdb-search-item.model';

@Resolver(() => ImdbSearchItemModel)
export class AuthorsResolver {
  constructor(private movieService: MovieService) {}

  @Query(() => [ImdbSearchItemModel])
  movieSearch(
    @Args('searchStr', { type: () => GraphQLString }) searchString: string,
  ) {
    return this.movieService.search(searchString);
  }
}
