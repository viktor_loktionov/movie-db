import { HttpModule, Module } from '@nestjs/common';
import { AuthorsResolver } from './movie.resolver';
import { MovieService } from './movie.service';
import { ImdbApiProvider } from './imdb/imdb.api-provider';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [HttpModule, ConfigModule],
  providers: [AuthorsResolver, MovieService, ImdbApiProvider],
  exports: [],
})
export class MovieModule {}
