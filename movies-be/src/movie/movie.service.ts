import { Injectable } from '@nestjs/common';
import { ImdbApiProvider } from './imdb/imdb.api-provider';

@Injectable()
export class MovieService {
  constructor(private imdbApi: ImdbApiProvider) {}

  search(searchString: string) {
    return this.imdbApi.search(searchString);
  }

  findOneById(id: number) {
    return {
      id,
      title: 'test',
    };
  }
}
