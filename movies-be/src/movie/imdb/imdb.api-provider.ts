import { HttpService, Injectable } from '@nestjs/common';
import { ImdbApiUrlEnum } from './imdb-api-url.enum';
import { ImdbApiSearchMovieItemInterface } from '../../contracts/imdb-search-item.interface';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ImdbApiProvider {
  private apiKey = '';
  private apiUrl = '';

  constructor(private httpService: HttpService, private config: ConfigService) {
    this.apiUrl = this.config.get<string>('IMDB_URL');
    this.apiKey = this.config.get<string>('IMDB_API_KEY');

    console.log(this.apiUrl);
  }

  public async search(searchString: string) {
    const response = await this.request({
      [ImdbApiUrlEnum.Search]: searchString,
    });

    return ImdbApiProvider.adaptApiAnswer(response.data).Search;
  }

  private request(params: Record<string, string>) {
    const paramsString = Object.entries(params)
      .map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
      .join('&');

    return this.httpService
      .get(`${this.apiUrl}?${paramsString}&apikey=${this.apiKey}`)
      .toPromise();
  }

  private static adaptApiAnswer(apiAnswer) {
    return {
      ...apiAnswer,
      Search: apiAnswer.Search.map((item: ImdbApiSearchMovieItemInterface) => ({
        title: item.Title,
        year: item.Year,
        type: item.Type,
        imdbId: item.imdbID,
        poster: item.Poster,
      })),
    };
  }
}
