import { SearchMovieItemInterface } from '../../contracts/imdb-search-item.interface';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ImdbSearchItemModel implements SearchMovieItemInterface {
  @Field()
  title!: string;

  @Field()
  year!: string;

  @Field()
  imdbID!: string;

  @Field()
  type!: string;

  @Field()
  poster: string;
}
