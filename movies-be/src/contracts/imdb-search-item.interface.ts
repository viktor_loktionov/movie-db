export interface ImdbApiSearchMovieItemInterface {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}

export interface SearchMovieItemInterface {
  title: string;
  year: string;
  imdbID: string;
  type: string;
  poster: string;
}
