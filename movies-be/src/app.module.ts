import { join } from 'path';

import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { MovieModule } from './movie/movie.module';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd() + '/src/schema.gql'),
      sortSchema: true,
      include: [MovieModule],
    }),
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    MovieModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
