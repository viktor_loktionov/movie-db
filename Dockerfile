FROM node:14

WORKDIR /app

COPY package.json ./
COPY movies-be/package.json ./movies-be/package.json
RUN yarn install

COPY . .

RUN yarn workspace movies-be build

ARG IMDB_URL=http://www.omdbapi.com/
ARG IMDB_API_KEY

ENV IMDB_URL=$IMDB_URL
ENV IMDB_API_KEY=$IMDB_API_KEY

CMD node movies-be/dist/main.js
